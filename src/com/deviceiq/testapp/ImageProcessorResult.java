package com.deviceiq.testapp;

import java.io.File;

import org.opencv.core.Mat;

public class ImageProcessorResult {
    public Boolean CassetteFound;
    public Boolean OrientationFound;
    public Boolean AssayWindowFound;
    public String ProcessorResult_A;
    public String ProcessorResult_B;
    public String ProcessorResult_C;

    public Mat Output;
    public String FileReference;

    public Exception Exception;

    public ImageProcessorResult() {
        CassetteFound = null;
        OrientationFound = null;
        AssayWindowFound = null;
        ProcessorResult_A = null;
        ProcessorResult_B = null;
        ProcessorResult_C = null;
        Output = null;
        FileReference = null;
    }

    public ImageProcessorResult(Boolean cassetteFound, Boolean orientationFound, Boolean assayWindowFound, String processorResultA, String processorResultB, String processorResultC, Mat output, String fileRef) {
        CassetteFound = cassetteFound;
        OrientationFound = orientationFound;
        AssayWindowFound = assayWindowFound;
        ProcessorResult_A = processorResultA;
        ProcessorResult_B = processorResultB;
        ProcessorResult_C = processorResultC;
        Output = output;
        FileReference = fileRef;
    }

    public String ConvertToString() {

        String file = new File(FileReference).getName();

        String output = String.format("Result: %1$s\nCassette Found: %2$s\nOrientation Found: %3$s\nAssay Window Found: %4$s\nFile Reference: %5$s",
                (ProcessorResult_A + "/" + ProcessorResult_B + "/" + ProcessorResult_C),
                CassetteFound ? "true" : "false",
                OrientationFound ? "true" : "false",
                AssayWindowFound ? "true" : "false",
                file);

        return output;
    }
}
