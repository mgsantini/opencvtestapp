package com.deviceiq.testapp;

import javax.imageio.ImageIO;
import javax.swing.*;

import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;

import java.awt.Image;
import java.io.*;
import java.util.prefs.Preferences;
import java.awt.event.*;

import javax.swing.*;
import java.io.*;
import java.awt.event.*;

public class ImageViewer {

	public static void main(String[] args) {

		JFrame frame = new ImageViewerFrame();
		frame.setTitle("Image Viewer App");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

	};
}

@SuppressWarnings("serial")
//A frame with a label to show an image
class ImageViewerFrame extends JFrame {

	JLabel label;
	JFileChooser chooser;
	JMenuBar menubar;
	JMenu menu;
	JMenu steps;
	JMenuItem open;
	JMenuItem processActive;
	JMenuItem openLast;
	JMenuItem processLast;
	JMenuItem exit;
	
	JMenuItem resizeImage;
	JMenuItem findCassette;
	JMenuItem findAssay;
	JMenuItem findSlice;
	JMenuItem findOrientationCircle;
	JMenuItem drawRectangles;
	JMenuItem drawCircles;
	JMenuItem cannyEdgeDetection;
	JMenuItem dilate;
	JMenuItem erode;
	JMenuItem threshold;
	JMenuItem medianBlur;
	JMenuItem gaussianBlur;
	JMenuItem findContours;
	JMenuItem convertToGray;
	JMenuItem convertToRgb;
	JMenuItem findAllRectangles;
	JMenuItem findAllCircles;
	
	String originalFile;
	String modifiedFile;
	
	String representedImage;
	
	Rect lastRectangleFound;
	
	String stepFile;
	
	public void PostAction(Mat matToWrite) {
		Imgcodecs.imwrite(modifiedFile, matToWrite);
    	matToWrite.release();
    	
		ImageIcon icon = new ImageIcon(modifiedFile);
		icon.getImage().flush();
		label.setIcon( icon );
		
		representedImage = modifiedFile;
	}
	
	JFrame thisFrame;
	
	// Constructor
	public ImageViewerFrame() {

		thisFrame = this;
		
		Preferences prefs = Preferences.userRoot().node(getClass().getName());

		setSize(1250, 750);

		// Use a label to display the image
		label = new JLabel();
		add(label);

		// Set up the file chooser
		chooser = new JFileChooser();
		chooser.setCurrentDirectory(new File("c:\\Temp\\"));

		// set up the menubar
		menubar = new JMenuBar();
		setJMenuBar(menubar);

		menu = new JMenu("File");
		menubar.add(menu);

		open = new JMenuItem("Open...");
		menu.add(open);

		processActive = new JMenuItem("Process Active File");
		menu.add(processActive);

		menu.addSeparator();
		
		openLast = new JMenuItem("Open Last File");
		menu.add(openLast);
		
		processLast = new JMenuItem("Process Last File");
		menu.add(processLast);

		steps = new JMenu("Steps");
		menubar.add(steps);
		
		resizeImage = new JMenuItem("Resize Image");
		steps.add(resizeImage);
		
		findCassette = new JMenuItem("Find Cassette");
		steps.add(findCassette);
		
		steps.addSeparator();
		
		findAssay = new JMenuItem("Find Assay");
		steps.add(findAssay);
		
		steps.addSeparator();
		
		findSlice = new JMenuItem("Find Slice");
		steps.add(findSlice);
		
		steps.addSeparator();
		
		findOrientationCircle = new JMenuItem("Find Orientation Circle");
		steps.add(findOrientationCircle);
		
		steps.addSeparator();
		
		convertToGray = new JMenuItem("Convert to Gray");
		steps.add(convertToGray);
		
		convertToRgb = new JMenuItem("Convert to RGB");
		steps.add(convertToRgb);
		
		steps.addSeparator();
		
		cannyEdgeDetection = new JMenuItem("Canny Edge Detection");
		steps.add(cannyEdgeDetection);
		
		dilate = new JMenuItem("Dilate");
		steps.add(dilate);
		
		erode = new JMenuItem("Erode");
		steps.add(erode);
		
		threshold = new JMenuItem("Adaptive Threshold");
		steps.add(threshold);
		
		medianBlur = new JMenuItem("Median Blur");
		steps.add(medianBlur);
		
		gaussianBlur = new JMenuItem("Gaussian Blur");
		steps.add(gaussianBlur);
		
		steps.addSeparator();
		
		findAllRectangles = new JMenuItem("Find All Rectangles");
		steps.add(findAllRectangles);
		
		findAllCircles = new JMenuItem("Find All Circles");
		steps.add(findAllCircles);
		
		openLast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					String lastUsedFile = prefs.get("LAST", "");

					if (lastUsedFile.length() > 0) {
						label.setIcon(new ImageIcon(lastUsedFile));	
						
						originalFile = lastUsedFile;
						representedImage = lastUsedFile;
						
						thisFrame.setTitle("Image Viewer App");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		processLast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try 
				{	
					openLast.doClick();
					
					// placeholder for new filename
					modifiedFile = originalFile.replace(".jpg", "_out.jpg");
										
					// convert to MAT and pass off to processor					
					ImageProcessorResult result = ImageProcessor.processImage(originalFile,  modifiedFile);
					
					thisFrame.setTitle("Image Viewer App (" + result.ProcessorResult_A + "/" + result.ProcessorResult_B + "/" + result.ProcessorResult_C + ")");
					PostAction(result.Output);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});

		processActive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				// process
				try {				
					// placeholder for new filename
					modifiedFile = originalFile.replace(".jpg", "_out.jpg");
										
					// convert to MAT and pass off to processor					
					ImageProcessorResult result = ImageProcessor.processImage(originalFile,  modifiedFile);

					// Set the modified image file (edited by processor) to be visible
					//label.setIcon(new ImageIcon(modifiedFile));
					//prefs.put("LAST", originalFile);
					
					//Mat src = ImageProcessor.convertToMat(modifiedFile);	
					
					thisFrame.setTitle("Image Viewer App (" + result.ProcessorResult_A + "/" + result.ProcessorResult_B + "/" + result.ProcessorResult_C + ")");
					PostAction(result.Output);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		open.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				// show file chooser dialog
				int result = chooser.showOpenDialog(null);

				// if file selected, set it as icon of label
				if (result == JFileChooser.APPROVE_OPTION) {
					String loadedFile = chooser.getSelectedFile().getPath();
					label.setIcon(new ImageIcon(loadedFile));
					
					originalFile = loadedFile;
					representedImage = loadedFile;
					
					prefs.put("LAST", loadedFile);
				}
				
				thisFrame.setTitle("Image Viewer App");
			}
		});
		
		findCassette.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				// process
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);					
					Mat ret = ImageProcessor.findCassette(src, representedImage);
					 
					modifiedFile = originalFile.replace(".jpg", "_cass.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		findAssay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				// process
				try {
					Mat src1 = ImageProcessor.convertToMat(representedImage);
					ImageProcessorResult result = new ImageProcessorResult();
					
					Rect rect1 = ImageProcessor.findAssayWindow(src1, representedImage, result);
					lastRectangleFound = rect1;
					
					Mat ret1 = ImageProcessor.drawRectangle(src1, representedImage, rect1, new Scalar(0, 0, 255, .8));					
					
					modifiedFile = originalFile.replace(".jpg", "_assay.jpg");
					
					PostAction(ret1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		findSlice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				// process
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);					
					
					Rect rect1 = ImageProcessor.findAssaySlice(src,  representedImage,  lastRectangleFound);
					Mat ret2 = ImageProcessor.drawRectangle(src, representedImage, rect1, new Scalar(255, 0, 0, .8));
					
					modifiedFile = originalFile.replace(".jpg", "_slice.jpg");
					
					PostAction(ret2);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		resizeImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				// process
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);					
					
					Mat ret = ImageProcessor.resizeImage(src,  representedImage);
					
					modifiedFile = originalFile.replace(".jpg", "_resized.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		findOrientationCircle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				// process
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);					
					
					double[] circle = ImageProcessor.findOrientationCircle(src,  representedImage);
					Mat ret = ImageProcessor.drawCircle(src, representedImage, circle);
					
					modifiedFile = originalFile.replace(".jpg", "_orient.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		cannyEdgeDetection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				// process
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);
					
					Mat ret = ImageProcessor.opcvCannyEdgeDetection(src);
					
					modifiedFile = originalFile.replace(".jpg", "_canny.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		medianBlur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				// process
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);
					
					Mat ret = ImageProcessor.opcvMedianBlur(src);
					
					modifiedFile = originalFile.replace(".jpg", "_median.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		gaussianBlur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				// process
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);
					
					Mat ret = ImageProcessor.opcvGaussianBlur(src);
					
					modifiedFile = originalFile.replace(".jpg", "_gaussian.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		convertToGray.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);

					Mat ret = ImageProcessor.opcvConvertToGray(src);
					
					modifiedFile = originalFile.replace(".jpg", "_gray.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		convertToRgb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);

					Mat ret = ImageProcessor.opcvConvertToRgb(src);
					
					modifiedFile = originalFile.replace(".jpg", "_rgb.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		dilate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);

					Mat ret = ImageProcessor.opcvDilate(src);
					
					modifiedFile = originalFile.replace(".jpg", "_dilate.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		erode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);

					Mat ret = ImageProcessor.opcvErode(src);
					
					modifiedFile = originalFile.replace(".jpg", "_erode.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		threshold.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);

					Mat ret = ImageProcessor.opcvAdaptiveThreshold(src);
					
					modifiedFile = originalFile.replace(".jpg", "_threshold.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		findAllRectangles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);

					Mat ret = ImageProcessor.drawRectangles(src, representedImage);
					
					modifiedFile = originalFile.replace(".jpg", "_wRects.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		findAllCircles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				try {
					Mat src = ImageProcessor.convertToMat(representedImage);

					Mat ret = ImageProcessor.drawCircles(src, representedImage);
					
					modifiedFile = originalFile.replace(".jpg", "_wCircs.jpg");
					
					PostAction(ret);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
}
