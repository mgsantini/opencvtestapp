package com.deviceiq.testapp;

import static org.opencv.core.Core.BORDER_DEFAULT;

import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.Core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class ImageProcessor {

// Android OPEN_CV init
//    static {
//        // initialize OpenCV for Android
//        boolean isSuccess = OpenCVLoader.initDebug();
//    }

    // Java OPEN_CV init
     static {
          // initialize OpenCV
          System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
     }

    private static final boolean DEV_MODE = true;

    // presets to trim assay slice down
    private static double ASSAY_SLICE_TRIM_MARGIN = .10;
    private static double ASSAY_SLICE_HEIGHT = 20;

    // presets to find the position of assay window if we couldn't find it dynamically
    private static double CASSETTE_REL_HEIGHT = .66;
    private static double ASSAY_WINDOW_REL_WIDTH_FROM_HEIGHT = .43;
    private static double ASSAY_WINDOW_FROM_TOP = .36;
    private static double ASSAY_WINDOW_FROM_LEFT = .5;
    private static double ASSAY_WINDOW_REL_HEIGHT = .3;
    private static double ASSAY_WINDOW_REL_WIDTH = .32;

    // image width to use when resizing
    private static double IMAGE_MAX_WIDTH = 1280.0;

    // thresholds to respect when setting L/M/H
    private static Scalar COLOR_DETECTION_SCALAR_HIGH = new Scalar(160, 160, 160); // RGB
    private static Scalar COLOR_DETECTION_SCALAR_MEDIUM = new Scalar(195, 195, 195); // RGB
    private static Scalar COLOR_DETECTION_SCALAR_LOW = new Scalar(230, 230, 230); // RGB

    // Setting this will make the processing routine not dependent on finding the orientation circle
    // This assume you've taken the picture with the correct
    // orientation and are not concerned with checking for it
    private static Boolean ALLOW_ORIENTATION_FAILURE = true;

    // when running from Android device
    private static String BASE_PATH = "/data/data/com.sekisui.mlfr.android/app_imageDir/";
    // when running from Java test harness
    // private static String BASE_PATH = Environment.getExternalStoragePublicDirectory();

    // Resize a given image and save to file system using the temp file name passed
    // in
    // New size should be no wider than 1280px and height stays relative to original
    public static Mat resizeImage(Mat src, String tempFile) {

        try {
            Mat clone = new Mat();

            double reduceFactor = IMAGE_MAX_WIDTH / src.width();
            double newHeight = src.height() * reduceFactor;

            Imgproc.resize(src, clone, new Size(IMAGE_MAX_WIDTH, newHeight));

            commitImage(clone, tempFile);

            clone.release();

            // reload the image from file
            src = convertToMat(tempFile);
        } catch (Exception ex) {
            // unable to resize image
            Log.e("Sekisui.MLFR", "Exception occurred while trying to resize image", ex);

            // rethrow exception to be caught at UI level
            throw ex;
        }

        return src;
    }

    private static Mat commitImage(Mat src, String file) {

        try {
            Mat outputMat = src.clone();

            Imgcodecs.imwrite(file, outputMat);
            outputMat.release();

            Mat newImage = Imgcodecs.imread(file);

            return newImage;
        } catch (Exception ex) {
            // unable to commit image
            Log.e("Sekisui.MLFR", "Exception occurred while trying to save image", ex);

            // rethrow exception to be caught at UI level
            throw ex;
        }
    }

    public static Mat convertToMat(String path) {
        try {
            Mat src = Imgcodecs.imread(path);

            return src;
        } catch (Exception ex) {
            // unable to convert to mat
            Log.e("Sekisui.MLFR", "Exception occurred while trying to convert image to MAT", ex);

            // rethrow exception to be caught at UI level
            throw ex;
        }
    }

    //region Image Processing routines
    public static ImageProcessorResult processImage(String origFile, String tempFile) throws Exception {

        ImageProcessorResult result = new ImageProcessorResult();

        try {
            Mat src = convertToMat(origFile);

            if (src.width() > IMAGE_MAX_WIDTH) {
                src = resizeImage(src, tempFile);
            }

            // find cassette routine
            Mat cassetteOnly = findCassette(src, tempFile);
            Mat clone = new Mat();

            if (cassetteOnly.empty() || cassetteOnly.height() <= 0 || cassetteOnly.width() <= 0) {
                // no cassette found
                result = new ImageProcessorResult(false, null, null, null, null, null, src, tempFile);
            } else {
                result.CassetteFound = true;

                if (cassetteOnly.height() > cassetteOnly.width()) {
                    // picture is portrait, needs to be landscape
                    Core.flip(cassetteOnly, clone, 1);

                    commitImage(clone, tempFile);

                    src = clone;

                    clone.release();

                    // reload the image from file
                    cassetteOnly = convertToMat(tempFile);
                }

                Mat tempMat = commitImage(cassetteOnly, tempFile);
                cassetteOnly.release();

                // find rectangle routine
                Rect assayRect = findAssayWindow(tempMat, tempFile, result);
                if (assayRect != null && assayRect.x > 0 && assayRect.y > 0) {
                    Mat wAssay = drawRectangle(tempMat, tempFile, assayRect, new Scalar(0, 0, 255, .8));
                    Rect assaySlice = findAssaySlice(tempMat, tempFile, assayRect);
                    Mat wRectangle = drawRectangle(wAssay, tempFile, assaySlice, new Scalar(255, 0, 0, .8));
                    tempMat = commitImage(wRectangle, tempFile);
                    wAssay.release();
                    wRectangle.release();

                    // find circles routine
                    double[] circle = findOrientationCircle(tempMat, tempFile);

                    if (circle == null || circle.length == 0) {
                        // we didn't find the orientation circle
                        // rotate 180 degrees to see if that changes anything
                        Mat flipTest = tempMat.clone();

                        Core.rotate(tempMat, flipTest, Core.ROTATE_90_CLOCKWISE);

                        double[] circleTest = findOrientationCircle(flipTest, tempFile);

                        if (circleTest != null && circle.length > 0) {
                            circle = circleTest;

                            tempMat = commitImage(flipTest, tempFile);
                            flipTest.release();
                        }
                    }

                    if ((circle == null || circle.length == 0) && !ALLOW_ORIENTATION_FAILURE) {
                        // no orientation found
                        result.OrientationFound = false;
                    } else {
                        if ((circle == null || circle.length == 0)) {
                            // store this result for the return object
                            result.OrientationFound = false;
                        } else {
                            result.OrientationFound = true;

                            Mat wCircle = drawCircle(tempMat, tempFile, circle);
                            tempMat = commitImage(wCircle, tempFile);
                            wCircle.release();
                        }

                        // circle was found, proceed to split the assay slice
                        Rect[] parts = divideSlice(tempMat, tempFile, assaySlice);

                        if (parts != null && parts.length == 3) {
                            // is there a change in part 1
                            result.ProcessorResult_A = colorChangeInRect(tempMat, tempFile, parts[0], "A",
                                    COLOR_DETECTION_SCALAR_HIGH, COLOR_DETECTION_SCALAR_MEDIUM,
                                    COLOR_DETECTION_SCALAR_LOW);
                            result.ProcessorResult_B = colorChangeInRect(tempMat, tempFile, parts[1], "B",
                                    COLOR_DETECTION_SCALAR_HIGH, COLOR_DETECTION_SCALAR_MEDIUM,
                                    COLOR_DETECTION_SCALAR_LOW);
                            result.ProcessorResult_C = colorChangeInRect(tempMat, tempFile, parts[2], "C",
                                    COLOR_DETECTION_SCALAR_HIGH, COLOR_DETECTION_SCALAR_MEDIUM,
                                    COLOR_DETECTION_SCALAR_LOW);
                        }
                    }
                }

                // grab a slice of the rectangle
                result.Output = tempMat;
                result.FileReference = tempFile;
            }
        } catch (Exception ex) {
            // unable to process image
            Log.e("Sekisui.MLFR", "Exception occurred while trying to process image", ex);

            result.Exception = ex;
        }

        return result;
    }

    public static Mat findCassette(Mat src, String file) throws Exception {

        Rect rect = null;

        try {
            Mat blurred = opcvMedianBlur(src);

            Mat gray0 = new Mat(blurred.size(), CvType.CV_8U);
            Mat gray;

            List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
            List<Mat> blurredChannel = new ArrayList<Mat>();
            List<Mat> gray0Channel = new ArrayList<Mat>();
            MatOfPoint2f approxCurve;

            blurredChannel.add(blurred);
            gray0Channel.add(gray0);

            double maxArea = 0;
            int maxId = -1;

            for (int c = 0; c < 3; c++) {
                int ch[] = { c, 0 };
                Core.mixChannels(blurredChannel, gray0Channel, new MatOfInt(ch));

                gray = opcvDilate(gray0);
                gray = opcvCannyEdgeDetection(gray);

                Imgproc.findContours(gray, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

                for (MatOfPoint contour : contours) {
                    MatOfPoint2f temp = new MatOfPoint2f(contour.toArray());
                    approxCurve = new MatOfPoint2f();

                    double area = Imgproc.contourArea(contour);
                    Imgproc.approxPolyDP(temp, approxCurve, Imgproc.arcLength(temp, true) * 0.02, true);

                    // we are looking for the largest rectangle found in the image, which should be the cassette
                    if (approxCurve.total() == 4 && area >= maxArea) {
                        double maxCosine = 0;

                        List<Point> curves = approxCurve.toList();
                        for (int j = 2; j < 5; j++) {
                            double cosine = Math.abs(angle(curves.get(j % 4), curves.get(j - 2), curves.get(j - 1)));
                            maxCosine = Math.max(maxCosine, cosine);
                        }

                        if (maxCosine < 0.3) {
                            maxArea = area;
                            maxId = contours.indexOf(contour);
                        }
                    }
                }
            }

            if (maxId >= 0) {
                // we found a rectangle, it will be cropped out of the original
                rect = Imgproc.boundingRect(contours.get(maxId));
            }

            Mat croppedImage = src.clone();

            if (rect != null) {
                croppedImage = new Mat(src, rect).clone();
            }

            return croppedImage;
        } catch (Exception ex) {
            // unable to find cassette
            Log.e("Sekisui.MLFR", "Exception occurred while trying to find cassette within image", ex);

            throw ex;
        }
    }

    public static Rect findAssayWindow(Mat src, String file, ImageProcessorResult result) throws Exception {

        Rect returnRect = new Rect();

        try {
            Mat clone = src.clone();

            Integer totalHeight = clone.height();
            Integer totalWidth = clone.width();

            double maxArea = 0;

            // find all the rectangles in the image
            ArrayList<Rect> rectangles = findRectangles(clone);

            // iterate over them and find the one that is approximately in the position we want
            for (Iterator<Rect> i = rectangles.iterator(); i.hasNext();) {
                // is this rectangle vertically centered?
                Rect item = i.next();

                double x1 = item.tl().x;
                double x2 = item.br().x;
                double y1 = item.tl().y;
                double y2 = item.br().y;

                // confirm that the placement of this rectangle matches where we expect it,
                // roughly
                if ((x1 > (totalWidth / 2)) && // topLeft is over half way point horizontally
                        (y1 < (totalHeight / 2)) && // topLeft is under half way point vertically
                        (y2 > (totalHeight / 2)) && // bottomRight is over half way point vertically
                        (x1 < ((totalWidth / 4) * 3)) && (totalWidth > totalHeight)) // topLeft is under 3/4 point horizontally
                {
                    if (item.area() > maxArea) {
                        returnRect = item;
                        result.AssayWindowFound = true;
                        maxArea = item.area();
                    }
                }
            }

            // resize the returned rect (in case it is not the entire area)
            if (!returnRect.empty()) {
                // ensure the width matches up with the expectancy
                // width can be affected in recognition by the vertical lines
                // ensure width is height is X% of width
                double rHeight = returnRect.height;
                double rWidth = returnRect.width;

                if (rWidth < (rHeight * 2)) {
                    // ensure rWidth is X% wider than the height
                    returnRect.width = (int) (rHeight / ASSAY_WINDOW_REL_WIDTH_FROM_HEIGHT);
                }

                // draw the rectangle around the assay window for visual verification
                drawRectangle(clone, file, returnRect, new Scalar(255, 0, 0));
            }

            commitImage(clone, file);
            clone.release();

            // if we didn't find the rectangle, used correlative calculations to find it
            if (returnRect.empty()) {
                returnRect = findStaticAssayWindow(src, file);
                result.AssayWindowFound = false;
            }

            return returnRect;
        } catch (Exception ex) {
            // unable to find assay window
            Log.e("Sekisui.MLFR", "Exception occurred while trying to find assay window within cassette", ex);

            throw ex;
        }
    }

    public static Rect findAssaySlice(Mat src, String file, Rect assayRect) throws Exception {
        Rect assaySlice = new Rect();

        try {
            // grab a slice X pixels high for the assay window (and trim the left/right Y pixels)
            double x1 = assayRect.tl().x;
            double x2 = assayRect.br().x;
            double y1 = assayRect.tl().y;
            double y2 = assayRect.br().y;

            double trimMargin = ASSAY_SLICE_TRIM_MARGIN * assayRect.width;

            double new_y1 = ((y1 + y2) / 2) - (ASSAY_SLICE_HEIGHT / 2);
            double new_y2 = new_y1 + ASSAY_SLICE_HEIGHT;
            double new_x1 = x1 + trimMargin;
            double new_x2 = x2 - trimMargin;

            assaySlice = new Rect((int) new_x1, (int) new_y1, (int) (new_x2 - new_x1), (int) (new_y2 - new_y1));

        } catch (Exception ex) {
            // unable to determine slice of assay window
            Log.e("Sekisui.MLFR", "Exception occurred while trying to find slice of assay window", ex);

            throw ex;
        }

        return assaySlice;
    }

    public static double[] findOrientationCircle(Mat src, String file) throws Exception {
        try {
            double[] retCircle = null;

            Mat clone = src.clone();

            Integer totalHeight = src.height();
            Integer totalWidth = src.width();

            Mat threshold = opcvAdaptiveThreshold(clone);
            Mat canny = opcvCannyEdgeDetection(threshold);
            Mat circles = new Mat();

            Imgproc.HoughCircles(canny, circles, Imgproc.HOUGH_GRADIENT, 3, (threshold.rows() / 16));

            Mat wCircles = src.clone();

            for (int x = 0; x < circles.cols(); x++) {
                double[] c = circles.get(0, x);

                if (c[2] < 100) {
                    double minY = (totalHeight / 2) - 25;
                    double maxY = (totalHeight / 2) + 25;
                    double halfWidth = (totalWidth / 2);

                    if (c[1] > minY && c[1] < maxY && c[2] < 50 && c[0] < halfWidth) {
                        retCircle = c;

                        if (DEV_MODE) {
                            // if we are in dev mode, we are printing an image of the mat with
                            // the circles drawn
                            drawCircles(wCircles, file);
                        }
                    }
                }
            }

            if (DEV_MODE) {
                commitImage(wCircles, (BASE_PATH + "circles.jpg"));
            }

            return retCircle;
        } catch (Exception ex) {
            // unable to draw a given rectangle
            Log.e("Sekisui.MLFR", "Exception occurred while trying to find orientation circle", ex);

            throw ex;
        }
    }

    public static Mat drawCircles(Mat src, String tempFile) throws Exception {
        try {
            Mat clone = src.clone();
            Mat gray = src.clone();

            Imgproc.cvtColor(src, gray, Imgproc.COLOR_BGR2GRAY);
            Imgproc.threshold(gray, gray, 125, 200, Imgproc.THRESH_BINARY);

            Mat canny = opcvCannyEdgeDetection(gray);
            Mat circles = new Mat();

            Imgproc.HoughCircles(canny, circles, Imgproc.HOUGH_GRADIENT, 3, gray.rows() / 16);

            for (int x = 0; x < circles.cols(); x++) {
                double[] c = circles.get(0, x);

                if (c[2] > 0 && c[2] < 40) {
                    drawCircle(src, tempFile, c);
                }
            }

            clone.release();

            return src;
        } catch (Exception ex) {
            // unable to find orientation circle(s)
            Log.e("Sekisui.MLFR", "Exception occurred while trying to find orientation circle", ex);

            throw ex;
        }
    }
    //endregion

    //region Private Methods
    public static Mat drawRectangle(Mat src, String file, Rect rect, Scalar color) throws Exception {
        try {
            Imgproc.rectangle(src, rect, color);

            return src;
        } catch (Exception ex) {
            // unable to draw a given rectangle
            Log.e("Sekisui.MLFR", "Exception occurred while trying to draw a rectangle", ex);

            throw ex;
        }
    }

    public static Mat drawCircle(Mat src, String tempFile, double[] circle) throws Exception {
        try {
            if (circle != null) {
                Point center = new Point(Math.round(circle[0]), Math.round(circle[1]));

                // circle center
                Imgproc.circle(src, center, 1, new Scalar(0, 100, 100), 3, 8, 0);

                // circle outline
                int radius = (int) Math.round(circle[2]);
                Imgproc.circle(src, center, radius, new Scalar(255, 0, 255), 2, 8, 0);
            }

            return src;
        } catch (Exception ex) {
            // unable to draw orientation circle
            Log.e("Sekisui.MLFR", "Exception occurred while trying to draw orientation circle", ex);

            throw ex;
        }
    }

    private static Rect[] divideSlice(Mat src, String file, Rect slice) throws Exception {
        try {
            Mat me = new Mat(src, slice);

            if (DEV_MODE) {
                commitImage(me, (BASE_PATH + "rect_whole.jpg"));
            }

            int eachSliceWidth = slice.width / 3;
            int eachSliceX = (int) slice.x + 1; // want to be sure not to include the rectangle that we drew so go in 1 pixel
            int eachSliceY = (int) slice.y + 1; // want to be sure not to include the rectangle that we drew so go in 1 pixel

            Rect rect1 = new Rect(eachSliceX, eachSliceY, eachSliceWidth, slice.height - 2);
            Rect rect2 = new Rect((eachSliceX + eachSliceWidth), eachSliceY, eachSliceWidth, slice.height - 2);
            Rect rect3 = new Rect((eachSliceX + eachSliceWidth + eachSliceWidth), eachSliceY, (eachSliceWidth - 1), slice.height - 2);

            Rect[] allParts = new Rect[] { rect1, rect2, rect3 };

            if (DEV_MODE) {
                Mat me1 = new Mat(src, rect1);
                Mat me2 = new Mat(src, rect2);
                Mat me3 = new Mat(src, rect3);

                commitImage(me1, (BASE_PATH + "rect_pre1.jpg"));
                commitImage(me2, (BASE_PATH + "rect_pre2.jpg"));
                commitImage(me3, (BASE_PATH + "rect_pre3.jpg"));
            }

            return allParts;
        } catch (Exception ex) {
            // unable to divide slice into three parts
            Log.e("Sekisui.MLFR", "Exception occurred while trying to divide slice into three parts", ex);

            throw ex;
        }
    }

    private static String colorChangeInRect(Mat src, String file, Rect part, String assayPart, Scalar targetScalarHigh,
                                            Scalar targetScalarMedium, Scalar targetScalarLow) {
        boolean colorChange = false;
        String retVal = "N";

        try {
            List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

            Mat submat = src.submat(part);
            Mat gray = new Mat();

            Mat dest_HIGH = new Mat();
            Mat dest_MED = new Mat();
            Mat dest_LOW = new Mat();

            Mat hierarchy = new Mat();

            gray = opcvConvertToGray(submat);

            if (DEV_MODE) {
                commitImage(gray, (BASE_PATH + "rect_before" + assayPart + ".jpg"));
            }

            Core.inRange(gray, new Scalar(0, 0, 0), targetScalarHigh, dest_HIGH);
            Core.inRange(gray, new Scalar(0, 0, 0), targetScalarMedium, dest_MED);
            Core.inRange(gray, new Scalar(0, 0, 0), targetScalarLow, dest_LOW);

            if (DEV_MODE) {
                commitImage(dest_HIGH, (BASE_PATH + "rect_high" + assayPart + ".jpg"));
                commitImage(dest_HIGH, (BASE_PATH + "rect_med" + assayPart + ".jpg"));
                commitImage(dest_HIGH, (BASE_PATH + "rect_low" + assayPart + ".jpg"));
            }

            // run through HIGH checks
            contours = opcvFindContours(dest_HIGH);
            colorChange = !contours.isEmpty();
            retVal = colorChange ? "H" : "N";

            if (!colorChange) {
                // run through MED checks
                contours = opcvFindContours(dest_MED);
                colorChange = !contours.isEmpty();
                retVal = colorChange ? "M" : "N";
            }

            if (!colorChange) {
                // run through LOW checks
                contours = opcvFindContours(dest_LOW);
                colorChange = !contours.isEmpty();
                retVal = colorChange ? "L" : "N";
            }

        } catch (Exception ex) {
            // unable to significant color change
            Log.e("Sekisui.MLFR", "Exception occurred while trying to determine a color change in given rectangle", ex);

            throw ex;
        }

        return retVal;
    }

    private static Rect findStaticAssayWindow(Mat src, String file) throws Exception {
        Rect returnedRect = new Rect();

        try {
            Mat clone = src.clone();

            Integer totalHeight = clone.height();
            Integer totalWidth = clone.width();

            double newHeight = totalWidth * CASSETTE_REL_HEIGHT;

            Imgproc.resize(src, clone, new Size(totalWidth, newHeight), Imgproc.INTER_CUBIC);

            double rect_fromLeft = totalWidth * ASSAY_WINDOW_FROM_LEFT;
            double rect_fromTop = newHeight * ASSAY_WINDOW_FROM_TOP;
            double rect_width = totalWidth * ASSAY_WINDOW_REL_WIDTH;
            double rect_height = newHeight * ASSAY_WINDOW_REL_HEIGHT;

            returnedRect = new Rect((int) rect_fromLeft, (int) rect_fromTop, (int) rect_width, (int) rect_height);

            return returnedRect;
        } catch (Exception ex) {
            // unable to find a static assay window
            Log.e("Sekisui.MLFR", "Exception occurred while trying to find the static assay window", ex);

            throw ex;
        }
    }

    private static ArrayList<Rect> findRectangles(Mat src) {
        try {
            ArrayList<Rect> returnRect = new ArrayList<Rect>();
            ArrayList<Rect> returnedRects = new ArrayList<Rect>();

            Mat thresh = opcvAdaptiveThreshold(src);

            // find contours
            List<MatOfPoint> contours = opcvFindContours(thresh);

            // find appropriate bounding rectangles
            for (MatOfPoint contour : contours) {
                RotatedRect boundingRect = Imgproc.minAreaRect(new MatOfPoint2f(contour.toArray()));
                double rectangleArea = boundingRect.size.area();

                // test min ROI area in pixels
                if (rectangleArea > 100) {
                    Point rotated_rect_points[] = new Point[4];
                    boundingRect.points(rotated_rect_points);
                    Rect rect = Imgproc.boundingRect(new MatOfPoint(rotated_rect_points));

                    returnRect.add(rect);
                }
            }

            for (Rect tempRect : returnRect) {
                // firstly, ignore anything less than 10 pixels tall/wide
                if (tempRect.height > 10 && tempRect.width > 10) {
                    // then, also ignore anything starting from top left
                    // as it is likely just a slightly smaller cassette
                    if (tempRect.x > 10 && tempRect.y > 10) {
                        returnedRects.add(tempRect);
                    }
                }
            }

            return returnedRects;
        } catch (Exception ex) {
            // unable to find rectangles in image
            Log.e("Sekisui.MLFR", "Exception occurred while trying to find rectangles in image", ex);

            throw ex;
        }
    }

    private static double angle(Point p1, Point p2, Point p0) {
        double dx1 = p1.x - p0.x;
        double dy1 = p1.y - p0.y;
        double dx2 = p2.x - p0.x;
        double dy2 = p2.y - p0.y;
        return (dx1 * dx2 + dy1 * dy2) / Math.sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10);
    }

    //endregion

    //region OpenCV Wrappers
    public static Mat opcvCannyEdgeDetection(Mat src) {
        Mat canny = new Mat();

        Imgproc.Canny(src, canny, 10, 20, 3, true);

        return canny;
    }

    public static Mat opcvDilate(Mat src) {
        Mat dilate = new Mat();

        Imgproc.dilate(src, dilate, new Mat(), new Point(-1, -1), 1);

        return dilate;
    }

    public static Mat opcvErode(Mat src) {
        Mat erode = new Mat();
        int erosion_size = 5;

        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
                new Size(2 * erosion_size + 1, 2 * erosion_size + 1));

        Imgproc.erode(src, erode, element);

        return erode;
    }

    public static Mat opcvAdaptiveThreshold(Mat src) {
        Mat gray = opcvConvertToGray(src);
        Mat gBlur = opcvGaussianBlur(gray);

        Mat thresh = new Mat();

        Imgproc.adaptiveThreshold(gBlur, thresh, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 15, 5);

        return thresh;
    }

    public static Mat opcvMedianBlur(Mat src) {
        Mat blurred = new Mat();

        Imgproc.medianBlur(src, blurred, 9);

        return blurred;
    }

    public static Mat opcvGaussianBlur(Mat src) {
        Mat blurred = new Mat();

        Imgproc.GaussianBlur(src, blurred, new Size(3, 3), 5, 10, BORDER_DEFAULT);

        return blurred;
    }

    public static Mat opcvBlur(Mat src) {
        Mat blurred = new Mat();

        Imgproc.blur(src, blurred, new Size(3, 3));

        return blurred;
    }

    public static Mat opcvConvertToGray(Mat src) {
        Mat gray = new Mat(src.size(), CvType.CV_8UC1);

        Imgproc.cvtColor(src, gray, Imgproc.COLOR_RGB2GRAY);

        return gray;
    }

    public static Mat opcvConvertToRgb(Mat src) {
        Mat rgb = new Mat();

        Imgproc.cvtColor(src, rgb, Imgproc.COLOR_BGR2RGB);

        return rgb;
    }

    public static ArrayList<MatOfPoint> opcvFindContours(Mat src) {
        ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();

        Imgproc.findContours(src, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        return contours;
    }
    //endregion

    // Used in the test Java App, keeping here for portability for now
    public static Mat drawRectangles(Mat src, String tempFile) throws Exception {
        try {
            Mat grayMat = src.clone();

            Imgproc.cvtColor(src, grayMat, Imgproc.COLOR_BGR2GRAY);
            Imgproc.threshold(grayMat, grayMat, 125, 200, Imgproc.THRESH_BINARY);

            // find contours
            List<MatOfPoint> whiteContours = new ArrayList<>();
            Imgproc.findContours(grayMat, whiteContours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

            Rect maxRect = new Rect();

            // find appropriate bounding rectangles
            for (MatOfPoint contour : whiteContours) {
                RotatedRect boundingRect = Imgproc.minAreaRect(new MatOfPoint2f(contour.toArray()));
                double rectangleArea = boundingRect.size.area();

                // test min ROI area in pixels
                if (rectangleArea > 100) {
                    Point rotated_rect_points[] = new Point[4];
                    boundingRect.points(rotated_rect_points);
                    Rect rect = Imgproc.boundingRect(new MatOfPoint(rotated_rect_points));

                    if ((rect.height) <= rect.width) {

                        double tl_y = rect.tl().y;
                        double br_y = rect.br().y;
                        double halfHeight = src.height() / 2;
                        double halfWidth = src.width() / 2;
                        double height = rect.height;
                        double width = rect.width;

                        if (rect.y < br_y && ((height * 2) > width) && tl_y < halfHeight && br_y > halfHeight
                                && rect.x > halfWidth) {
                            Rect r2 = rect;

                            if (r2.area() > maxRect.area()) {
                                // r2.width = r2.width + 20;
                                maxRect = r2;
                            }

                        }

                        drawRectangle(src, tempFile, rect, new Scalar(0, 0, 255));
                    }
                }
            }

            return src;
        } catch (Exception ex) {
            // unable to find rectangles in image
            Log.e("Sekisui.MLFR", "Exception occurred while trying to find rectangles in image", ex);

            throw ex;
        }
    }

}
